import Vue from 'vue';
import './plugins/vuetify'
import App from './App.vue';
import {store} from './store';
import axios from 'axios';
import VueAxios from 'vue-axios';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);

new Vue({
  render: h => h(App),
  store,
}).$mount('#app');
