import Vue from 'vue';
import Vuex from 'vuex';
import {people} from './modules/people';
import {films} from './modules/films';

Vue.use(Vuex);

export const store =  new Vuex.Store({
  modules: {
    people,
    films
  }
});

