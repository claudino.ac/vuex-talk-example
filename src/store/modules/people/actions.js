import Vue from 'vue';

const headers =  {
    'Content-Type': 'application/json; charset=utf8',
    'Access-Control-Allow-Origin': '*',
}

const params = {
}

export const actions = {
  getPeople(context) {
    Vue.axios.get('https://swapi.co/api/people',params, headers).then((response) => {
      let people = [];
      for(let person of response.data['results']){
         people.push(person);
      }
      context.commit('loadPeople', people);
    });
  }, 
  addPerson(context, person) {
    context.commit('addPerson', person);
  }
}
