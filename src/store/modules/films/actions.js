import Vue from 'vue';

const headers =  {
    'Content-Type': 'application/json; charset=utf8',
    'Access-Control-Allow-Origin': '*',
}

const params = {
}

export const actions = {
  getFilm(context, filmLink) {
    Vue.axios.get(filmLink,params, headers).then((response) => { 
      let films = context.state.films;
      let inList = films.filter((item) => {
          return item['url'] === filmLink
      });
      if(inList.length === 0){
        context.commit('addFilm', response.data);
      }
    });
  },
}
