import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    films: [],
};

const namespaced = true;

export const films = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};
