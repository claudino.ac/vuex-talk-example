import Vue from 'vue';

export const mutations = {
  addFilm(state, film){
    let films = state.films
    films.push(film);
    Vue.set(state, 'films', films);
  }

}
