export const getters = {
  filmsByPerson: (state) => (person) => {
    let films = []
      for (let film of person.films){
        let item = state.films.filter( item => {
          return item.url === film
        })
        if(item.length !== 0){
          films.push(item[0]);
        }
      }
      return films;
  }
}
